---
categories: []
date: 2016-06-01T11:49:45+01:00
description: ""
keywords: []
slug: irssi-in-docker
tags:
- docker
- irssi
- terminal-notifier
title: Irssi in docker with notifications on a Mac
---

Recently I've started playing with the [Docker for Mac beta](https://blog.docker.com/2016/03/docker-for-mac-windows-beta/) and during my search of what the $#@^ to do with it, came across this awesomeness: [docker containers on the desktop](https://blog.jessfraz.com/post/docker-containers-on-the-desktop/).  I'm not a total convert (too disorganised/busy/whatever) but have started poking the odd app into containers.

One of these was irssi, which I use with private irc servers.

So I used the [official irssi](https://hub.docker.com/_/irssi/) image, put it behind a [bash function](https://github.com/halberom/dotfiles/blob/master/.dockerfunc#L26) and voila!  Which was very nice and easy, just dump irssi config in ```$HOME/.irssi``` as per normal.

But then I decided I wanted to be notified when someone mentioned my nick...  I use a Mac, several solutions out on the interweb mention notify scripts and growl - but there's no real need to use growl with the excellently simple [terminal-notifier](https://github.com/julienXX/terminal-notifier). With that [installed](https://github.com/halberom/dotfiles/blob/master/Makefile#L26), the irssi [fnotify](http://github.com/rndstr/fnotify) script and a quick bash [hack](https://github.com/halberom/dotfiles/blob/master/bin/irssi_terminal_notifier) to poll the output, I was sorted.  All that was left was to start it when irssi was called, achieved by a simple [call](https://github.com/halberom/dotfiles/blob/master/.dockerfunc#L35) at the end of the bash function.

