---
categories:
- devops
date: 2015-05-22T15:28:10+01:00
description: ""
keywords: []
slug: ansible-modify-list
tags:
- ansible
- jinja
title: Ansible - modify all items in a list
---

*Update (2017-05-17)*

I realised today that there was a much easier way of doing this than my custom filter of 2 years ago, see the following gist

{{< gist halberom 9bfe009c9a3df5d64c7bb505a7700430 >}}


*Original*

Recently on #ansible in IRC, a person queried how to modify all items in a list.  The actual requirement had to do with having users [Alice, Bob, Carol], and needing to pass conf file names to another role in the form [Alice.conf, Bob.conf, Carol.conf].

I found that by default, it's not as easy as you might think.

In the end I created a simple custom filter

{{< gist halberom b1f6eaed16dba1b298e8 >}}

