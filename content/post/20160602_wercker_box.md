---
categories:
- devops
date: 2016-06-02T23:45:44+01:00
description: ""
keywords: []
slug: wercker-box
tags:
- docker
- hugo
- wercker
title: making my own wercker box to run hugo
---

In a [recent post]({{< ref "20160530_no_more_vps.md" >}}) I mentioned following the tutorial [Automated deployments with Wercekr](https://gohugo.io/tutorials/automated-deployments#creating-a-basic-hugo-site) to build and deploy my new Hugo generated blog.

It's an excellent post, which uses existing community steps to make things quick and easy to setup.

However something I noticed was that it's using a full debian container and so the the build steps were doing quite a lot of work. e.g.

``` bash
...
The following extra packages will be installed:
  file libexpat1 libffi6 libmagic1 libpython-stdlib libpython2.7-minimal
  libpython2.7-stdlib libsqlite3-0 libssl1.0.0 mime-support python
  python-chardet python-minimal python-pkg-resources python2.7
  python2.7-minimal
Suggested packages:
  python-doc python-tk python-distribute python-distribute-doc
  ttf-bitstream-vera python2.7-doc binutils binfmt-support
The following NEW packages will be installed:
  file libexpat1 libffi6 libmagic1 libpython-stdlib libpython2.7-minimal
  libpython2.7-stdlib libsqlite3-0 libssl1.0.0 mime-support python
  python-chardet python-minimal python-pkg-resources python-pygments python2.7
  python2.7-minimal
0 upgraded, 17 newly installed, 0 to remove and 0 not upgraded.
Need to get 6648 kB of archives.
After this operation, 27.3 MB of additional disk space will be used.
...
```

Being curious about the whole box/container setup required, I decided to replace the box used in the tutorial (debian), and the steps provided [arjen/hugo-build](https://github.com/ArjenSchwarz/wercker-step-hugo-build) with my own.

I started by installing [wercker-cli](http://wercker.com/cli/)

``` bash
brew tap wercker/wercker
brew install wercker-cli
```

Created a [hugo](https://hub.docker.com/r/halberom/hugo/) docker container, and pushed it to the [hub](https://hub.docker.com/r/halberom/hugo/).

And then updated my wercker.yml file to use it, and to have a dev section.

``` yaml
box: halberom/hugo
dev:
  steps:
    - internal/watch:
        code: hugo server
```

Followed by trying it out! (and crossing fingers it would work)

``` bash
wercker dev
```

sadly not the case

``` bash
--> Executing pipeline
--> Running step: setup environment
Pulling from halberom/hugo: latest
Already exists: 420890c9e918
Already exists: e4d561447a5c
Already exists: f27a57ccb1c2
Already exists: 1965ea271a7c
Digest: sha256:76af88bd222a8b6859c26ea696c096627d259042cf05004ef19d4c3de44906c5
Status: Image is up to date for halberom/hugo:latest
ERROR --> Step failed: setup environment 2.73s
Command cancelled due to error
ERROR Command cancelled due to error
WARNING Box container has already stopped.
FATAL Exiting.
```

Thus ensued a bit of --debug/--verbose usage, and ultimately

``` bash
DEBU[0002]   runner| Setting up guest (base box)
INFO[0002]  literal| [ ]  stdin "mkdir -p \"/pipeline/output\"\n"
INFO[0002]  literal| [ ]  stdin "echo 91de967b-0050-40c9-93a2-c21457c3d78e $?\n"
DEBU[0002] dockertr| Container finished with status code: 127 1014921707a1284f00de616a8b1b2009cf4cecfae4ff825a2ca8707570890115
```

Some searching around that, and I found the following 2 very useful links

 * https://github.com/wercker/wercker/issues/6
 * http://devcenter.wercker.com/docs/faq/alpine-faq.html

So then my wercker.yml file became

``` yaml
box:
  id: halberom/hugo
  cmd: /bin/sh
dev:
  steps:
    - internal/watch:
        cmd: /bin/sh
        code: hugo server
build:
  steps:
    - script:
        name: run hugo
        code: |
          hugo
deploy:
  box: debian
  steps:
    - install-packages:
        packages: git ssh-client
    - lukevivier/gh-pages@0.2.1:
        token: $GIT_TOKEN
        domain: blog.halberom.co.uk
        basedir: public
```

Testing worked, with several new folders created locally \_builds, \_steps, \_projects, and I was able to find (under \_projects) the generated output.

The new setup means I can now run ```wercker dev|build``` locally and can easily see exactly what wercker is doing behind the scenes, in terms of dir structures and ENV variables, with verbose/debug enabled.

What's also nice, is that now my build stage now only takes 9 seconds, vs approx 45 seconds or more previously.

Note that for now I'm sticking with the tutorial's deploy steps, which means I still need the debian container. Next steps will be looking at that, and also finishing my hugo docker image so it's a bit tidier.


