---
categories:
- devops
date: 2015-03-20T14:50:26+01:00
description: ""
keywords: []
slug: ansible-union-filter
tags:
- ansible
title: Ansible - union filter
---

You have a comma separated list of items, or an actual list, and you want to add some items to it only if they aren't already in it. The easiest way is with the [union](http://docs.ansible.com/playbooks_filters.html#set-theory-filters) filter.

In the following gist, we have a mount with some options that we want to add to others

{{< gist halberom ce2e36e1b13585792795 >}}
