---
categories:
- linux
date: 2014-03-23T13:42:25+01:00
description: ""
keywords: []
slug: shutter
tags:
- screenshot
- shutter
- documentation
title: shutter
---

Grabbing images to add to documentation, and highlighting relevant sections, or redacting (through pixelating) to hide confidential information is easy with [Shutter](http://shutter-project.org/)

I recommend [Skitch]({filename}../tools/skitch.md) if using Windows or Mac.
