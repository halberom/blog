---
categories:
- linux
date: 2014-08-09T18:48:29+01:00
description: Bash prompt with GIT branch
keywords: []
slug: bash-git-prompt
tags:
- bash
- git
title: bash git prompt
---

In a team environment, it's common to use GIT branches.  However this means you need to pay more attention to what branch you're in before doing commits and pushes.  You can of course use the git branch command.

{{< gist halberom c0285379a04e30ba13488e7cfaaac51c >}}

However you might skip that step and accidentally plonk your new *untested* code directly into master or similar.  Much better then to improve visibility by adding the current branch to your bash prompt.

You'll need bash_completion and git installed.

Edit your bashrc

``` bash
vim ~/.bashrc
```

Add a section to import the relevant git bash_completion file

``` bash
# linux
if [ -f /etc/bash_completion.d/git ]; then
. /etc/bash_completion.d/git
fi
# mac
#if [ -f /usr/local/git/contrib/completion/git-prompt.sh ]; then
#  . /usr/local/git/contrib/completion/git-prompt.sh
#fi
```

Update your git prompt to something like the following

``` bash
# linux
# show pending (+ = new files, * = changed files) state
export GIT_PS1_SHOWDIRTYSTATE=1
export PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[0;34m\]\w\[\e[m\]\[\e[0;31m\]$(__git_ps1)\[\e[m\] $ '
```

Source the file

``` bash
# linux
source ~/.bashrc
```

And browse to a path with a git repo
