---
categories:
- linux
date: 2014-03-24T21:13:23+01:00
description: ""
keywords: []
slug: mod_reqtimeout
tags:
- apache
- mod_reqtimeout
title: mod_reqtimeout
---

While mucking around with mod\_proxy and mod\_proxy\_connect using Apache 2.4 (httpd24) on CentOS, I found I was getting connections dropped at frequent intervals with the client side error 

``` bash
Write failed: Broken pipe
```

Enabling debugging on the server side gave me this output

``` bash
# ip's redacted
[Mon Mar 24 22:49:31.775113 2014] [reqtimeout:info] [pid 6626] [client 1.2.3.4:62147] AH01382: Request header read timeout
[Mon Mar 24 22:49:31.775193 2014] [ssl:info] [pid 6626] (70007) The timeout specified has expired: [client 1.2.3.4:62147] AH01991: SSL input filter read failed.
[Mon Mar 24 22:49:31.775211 2014] [proxy_connect:debug] [pid 6626] mod_proxy_connect. c(183):(70007)The timeout specified has expired: [client 1.2.3.4:62147] AH01018: error on client - ap_get_brigade
```

Which led me in the direction of [mod_reqtimeout](http://httpd.apache.org/docs/2.4/mod/mod_reqtimeout.html).  Why I hadn't come across this previously became apparent when I noticed this bit in the directive: 'Compatibility: ...;  defaulted to _disabled in version 2.3.14 and earlier_.'

I was able to stop the timeout by adding the following to my VirtualHost

``` bash
RequestReadTimeout header=0,MinRate=500 body=0,MinRate=500
```

However, this isn't ideal because it defeats the purpose of the mod - that is, to prevent DDoS attacks like [Slowloris](http://en.wikipedia.org/wiki/Slowloris)

The mod gets applied at the contexts: 'server config, virtual host', so can't be set for a specific category.
