---
categories:
- devops
date: 2016-05-30T21:26:13+01:00
description: ""
keywords: []
slug: no_more_vps
tags:
- github
- hugo
- irssi
- pelican
- route53
- wercker
title: RIP my old t1.micro, moved to irccloud and github pages
---

I recently decided to tidy up some of my sprawling mess of 1's and 0's, the main culprit being a 3 year old ec2 t1.micro running

   * irssi in screen (using [ssh into screen]({{< ref "post/20140323_ssh_screen.md" >}}))
   * nginx running my [pelican](http://blog.getpelican.com/) generated blog

It needed to go as I wasn't managing it in an automated fashion (tsk), wasn't backing it up beyond the occasional snapshot (tsk) and it was costing approx £7-15 per month which I felt could be reduced.  (Yes I was using an on-demand instance, another tsk)

With a bit of google foo, and some trial and error, I've settled on the following

* [irccloud.com](http://irccloud.com), for a reasonable price it gives me history and web+device access.
* [hugo](http://gohugo.io), I'm curious about golang, and my previous blog was a bit of a hack job in terms of theme, content and git management so I wanted to start again.
* [github pages](https://pages.github.com/), for running my blog, it's free and is easy to setup/use

One of the main reasons I flipped to hugo was I read the tutorial [Automated deployments with Wercker](https://gohugo.io/tutorials/automated-deployments#creating-a-basic-hugo-site) and being interested in CI/CD, wanted to give it a try. So [Wercker](http://wercker.com/) has been added to the mix.

My costs have been reduced to approx £3 per month (for irccloud), I've got [CI](https://en.wikipedia.org/wiki/Continuous_integration) and [CD](https://en.wikipedia.org/wiki/Continuous_delivery) by using Wercker, and my blog has been redone in a nice tidy way that hopefully I can remember how to manage.  Overall a success.
