---
categories:
- linux
date: 2014-03-23T21:21:35+01:00
description: ""
keywords: []
slug: screenrc
tags:
- screenrc
title: screenrc
---

Using [GNU Screen](http://www.gnu.org/software/screen/) (or its alternative [Tmux](http://tmux.sourceforge.net/)) is pretty much mandatory for any \*nix admin.  However the default screen settings are rather empty giving you nothing more than a blank screen with no indications of state.

I prefer a bit more information and like to use the following

``` bash
# ~/.screenrc
termcapinfo xterm* ti@:te@
startup_message off
vbell off
autodetach on
altscreen on
shelltitle '$ |bash'
shell bash
defscrollback 10000
defutf8 on
nonblock on
escape ^Jj
hardstatus on
hardstatus alwayslastline
hardstatus string "%{= kG}%-w%{.rW}%n %t%{-}%+w %=%{..G} %{B}%H %{..Y} %m/%d %C%a "
```

*Note the 'escape ^Jj'*, I use this rather than the default to allow using the bash shortuct 'ctrl-a' (to go to the beginning of a line).
