---
categories: []
date: 2015-03-20T13:52:58+01:00
description: ""
keywords: []
slug: ansible-ec2-route53
tags:
- ansible
- ec2
- route53
title: Ansible - adding multiple ec2 ip's to a dns record in route53
---

When creating multiple instances using the ec2 module, it returns a dict of instances.  If you then want to add those instances to route53, you need to somehow access the nested attributes to get the IP's and then return them in a comma separated string.

Voila!

``` yaml
---
- hosts: localhost
  connection: local
  tasks:
    - name: create some instances
      ec2:
        ...
      register: ec2

    - name: add all the instance ip addresses to route53
      route53:
        ...
        value: "{{ ec2.instances|map(attribute='public_ip')|join(',') }}"
```

I thought this was brilliant, as it was my first introduction to the wonderful world that is the jinja2 [map](http://jinja.pocoo.org/docs/dev/templates/#map) filter.  My thanks go to @no1peanut for replying with the example in #ansible.

