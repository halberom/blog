---
categories: []
date: 2014-03-23T15:45:59+01:00
description: ""
keywords: []
slug: flux
tags:
- flux
title: f.lux - saving eyeballs from screen glare
---

For those (late) evening computer sessions, use [f.lux](http://justgetflux.com/) to help your eyes by changing the warmth of your screen automatically based on sunset/sunrise schedules.
