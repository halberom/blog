---
categories:
- devops
date: 2015-03-18T13:56:55+01:00
description: ""
keywords: []
slug: ansible-get-installed-pkgs-module
tags:
- ansible
- rpm
title: Ansible - list installed packages with custom module
---

It's very easy to get a list of all installed packages on a RHEL box, you can create a custom module in the /library folder, see the following gist for an example

{{< gist halberom f4515e6b9d977f64294d >}}
