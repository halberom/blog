---
categories:
- devops
date: 2015-03-30T13:48:01+01:00
description: ""
keywords: []
slug: ansible-complex-variables
tags:
- ansible
title: Ansible - complex variables
---

I put together the following gist as an example of how to assign complex vars to variables or facts in ansible. Each of the plays outputs the same result.

{{< gist halberom e02b1d644d54352820e8 >}}
