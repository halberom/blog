---
categories:
- devops
date: 2015-07-15T23:19:56+01:00
description: ""
keywords: []
slug: ansible-vpc-nat-asg
tags:
- ansible
- aws
- ec2
- vpc
title: Ansible - create a multi-az VPC with auto scaling NAT instances
---

A few weeks ago I wrote about creating a pub/priv VPC. This is v2 of that attempt (using ansible v2 funnily enough).

I simplified it a bit, as I was trying to create multiple VPCs in one go and ended up with too much with_together and nesting. I also extended it to use auto scaling launch configuration and groups to handle the NAT instances, this way they're more robust.

The result can be found in the github repo [ansibe_ec2_vpc_nat_asg](https://github.com/HighOps/ansible_ec2_vpc_nat_asg/)

*Edit*: Since writing this piece, I've decided that [Terraform](https://www.terraform.io/) would be more suitable for managing the infrastructure. While I could get Ansible to do the job, it was a lot of work, with even more if I wanted the ability to tear-down my environments.  Nor could I do a dry-run and see what would change or what needed to change.
