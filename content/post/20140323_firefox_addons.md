---
categories:
- tools
date: 2014-03-23T15:41:48+01:00
description: ""
keywords: []
slug: firefox-addons
tags:
- addons
- firefox
title: Firefox add-ons
---

Everyone has their own style of working, but I find that with today's widescreen monitors moving the tabs to the left hand side in a tree (with clear inheritance between sites), and the bookmarks to the righthand side works best for me.

 * [Tree Style Tab](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/)
 * [RightBar](https://addons.mozilla.org/en-US/firefox/addon/rightbar/)

To show code trees in github and gitlab, the following is brilliant

 * [Octotree](https://github.com/buunguyen/octotree)

Additionally, I like to use the keyboard over the mouse as it's much faster

 * [Vimperator](https://addons.mozilla.org/en-US/firefox/addon/vimperator/)

And finally, with all the stories of hacks, government monitoring and what-not, a bit of security seems like a good idea

 * [Self-Destructing Cookies](https://addons.mozilla.org/en-US/firefox/addon/self-destructing-cookies/)
 * [FoxyProxy Standard](https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/) - _e.g. when used with an SSH tunnel_
