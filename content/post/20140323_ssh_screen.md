---
categories:
- linux
date: 2014-03-23T13:44:33+01:00
description: ""
keywords: []
slug: ssh-into-screen
tags:
- screen
- ssh
title: ssh into screen
---

There are a lot of occassions for using [GNU Screen](http://www.gnu.org/software/screen/), most of them assume you need the ability to disconnect while leaving something running long term (e.g. [irssi](http://www.irssi.org/)), and reconnecting at ad-hoc intervals. 

Instead of doing something like

``` bash
# local machine
ssh foo.example.com

# remote machine
screen -R bar
```

You can merge everthing into one command

``` bash
ssh -t foo.example.com screen -dR bar
```

This will create the screen socketname 'bar' if it doesn't exist, and will reconnect to it if it does.

A bash alias can be used for repeatability.

