---
categories:
- devops
date: 2016-06-11T12:34:37+01:00
description: ""
keywords: []
slug: wercker-box-2
tags:
- docker
- hugo
- wercker
title: making my own wercker box, part 2
---

Continuing on from [making my own wercker box to run hugo]({{< ref "20160602_wercker_box.md" >}}), I went to make a slight change to my site yesterday and got hit with

``` bash
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 git : Depends: liberror-perl but it is not going to be installed
E: Unable to correct problems, you have held broken packages.
```

This was in the 'install-packages' part of the [deploy pipeline](https://gohugo.io/tutorials/automated-deployments#adding-a-github-pages-step) defined in the wercker.yml file.

Evidently something had changed with the debian container and repositories. Which gave me the perfect execuse to quickly whip up my own alternative container.

So that I could control the deploy steps (and ensure they don't disappear on me), I forked [lvivier gh-pages steps](https://github.com/lvivier/step-gh-pages), and added a few changes based on the [wercker/step-slack](https://github.com/wercker/step-slack) repo configuration - most notably was the wercker.yml file which included a [shellcheck](https://github.com/halberom/step-gh-pages/blob/master/wercker.yml#L4) step that was quite useful for modifying the run.sh to be POSIX sh compliant. 

Output when testing with the shellcheck looked like this
``` bash
cd $WERCKER_SOURCE_DIR
$ export WERCKER_STEP_ROOT="/wercker/steps/wercker/shellcheck/1.0.0"
$ export WERCKER_STEP_ID="66af7643-3469-44a1-8296-b60b050ddb1e"
$ export WERCKER_STEP_NAME="shellcheck"
$ export WERCKER_REPORT_NUMBERS_FILE="$WERCKER_REPORT_DIR/$WERCKER_STEP_ID/numbers.ini"
$ export WERCKER_REPORT_MESSAGE_FILE="$WERCKER_REPORT_DIR/$WERCKER_STEP_ID/message.txt"
$ export WERCKER_REPORT_ARTIFACTS_DIR="$WERCKER_REPORT_DIR/$WERCKER_STEP_ID/artifacts"
$ mkdir -p $WERCKER_REPORT_ARTIFACTS_DIR
$ export WERCKER_STEP_TEMP="/tmp/$WERCKER_STEP_ID"
$ source '/wercker/wercker-build-essentials/init.sh'
$ mkdir -p $WERCKER_STEP_TEMP
$ export WERCKER_SHELLCHECK_FILES="run.sh"
$ export WERCKER_SHELLCHECK_SEPARATOR=" "
$ source "$WERCKER_STEP_ROOT/run.sh"
ShellCheck - shell script analysis tool
version: 0.3.5
license: GNU Affero General Public License, version 3
website: http://www.shellcheck.net

shellcheck run.sh

In run.sh line 13:
elif [ 'github.com' == "$WERCKER_GIT_DOMAIN" ]
                    ^-- SC2039: In POSIX sh, == is not supported.


In run.sh line 28:
cd $WERCKER_GH_PAGES_BASEDIR
     ^-- SC2086: Double quote to prevent globbing and word splitting.


In run.sh line 35:
if [ -n $WERCKER_GH_PAGES_DOMAIN ]
        ^-- SC2070: Always true because you failed to quote. Use [[ ]] instead.
        ^-- SC2086: Double quote to prevent globbing and word splitting.


In run.sh line 37:
echo $WERCKER_GH_PAGES_DOMAIN > CNAME
       ^-- SC2086: Double quote to prevent globbing and word splitting.


In run.sh line 43:
if [[ "$repo" =~ $WERCKER_GIT_OWNER\/$WERCKER_GIT_OWNER\.github\.(io|com)$ ]]; then
   ^-- SC2039: In POSIX sh, [[ ]] is not supported.
                                   ^-- SC1001: This \/ will be a regular '/' in this context.


In run.sh line 56:
result="$(git push -f $remote master:$branch)"
                      ^-- SC2086: Double quote to prevent globbing and word splitting.


In run.sh line 58:
if [[ $? -ne 0 ]]
   ^-- SC2039: In POSIX sh, [[ ]] is not supported.
```
_Making it POSIX sh compliant of course means that my alpine based containers no longer need bash installed._

Once I had that all cleaned up, all that was left to do was create a docker container with git installed - https://github.com/halberom/docker_git - and setup an automated build - https://hub.docker.com/r/halberom/git/.

My wercker file became:
``` yaml
box: 
  id: halberom/hugo
  cmd: /bin/sh
dev:
  steps:
    - internal/watch:
        code: hugo server
build:
  steps:
    - script:
        name: hugo check
        code: |
          hugo check
    - script:
        name: run hugo
        code: |
          hugo
deploy:
  box: 
    id: halberom/git
    cmd: /bin/sh
  steps:
    - halberom/gh-pages@0.2.1:
        token: $GIT_TOKEN
        domain: blog.halberom.co.uk
        basedir: public
```
_The hugo check isn't very useful, but I've started playing with validation and have thrown it in for now_

And my deploy time dropped from approx 50 seconds (20 for setup-env + 30 to install packages) to a delightful 3 seconds.

